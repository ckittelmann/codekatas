How to execute the samples:
- Get GHCI: https://www.haskell.org/platform/
- load the file using the command: ":l path/reinis_josef.hs"
- execute functions by using their name, e.g. "last1 [1,2,3]"