module CodingKata.P08Spec where

import Test.Hspec
import CodingKata.P08

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "Pack consecutive duplicates of list elements into sublists." $ do
    it "with single char" $ do
      compress ['a'] `shouldBe` ['a']

    it "with single int" $ do
      compress [1] `shouldBe` [1]

    it "with test sample" $ do
      compress ['a', 'a', 'a', 'a', 'b', 'c', 'c', 'a', 'a', 'd', 'e', 'e', 'e', 'e'] `shouldBe` ['a', 'b', 'c', 'a', 'd', 'e']