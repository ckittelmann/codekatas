﻿// Weitere Informationen zu F# unter "http://fsharp.net".
// Weitere Hilfe finden Sie im Projekt "F#-Lernprogramm".
// type 'a NestedList = List of 'a NestedList list | Elem of 'a

[<EntryPoint>]
let main args = 
    let list = [1; 1; 2; 3; 5; 8]
    (", ", list) 
    |> System.String.Join 
    |> printfn "%s"

    let nestedList = (Tasks.List [Tasks.Elem 1; Tasks.List [Tasks.Elem 2; Tasks.List [Tasks.Elem 3; Tasks.Elem 4]; Tasks.Elem 5]])
    let nestedListChars = (Tasks.List [Tasks.Elem "a"; Tasks.List [Tasks.Elem "b"; Tasks.List [Tasks.Elem "c"; Tasks.Elem "d"]; Tasks.Elem "e"]])

    System.Console.WriteLine()
    System.Console.WriteLine("task01")
    Tasks.task01 list
    System.Console.WriteLine("task02")
    Tasks.task02 list
    System.Console.WriteLine("task03")
    Tasks.task03 list 1
    System.Console.WriteLine("task04.1")
    Tasks.task041 list
    System.Console.WriteLine("task04.2")
    Tasks.task042 list
    System.Console.WriteLine("task04.3")
    Tasks.task043 list
    System.Console.WriteLine("task05.1")
    Tasks.task051 list
    System.Console.WriteLine("task05.2")
    Tasks.task052 list
    System.Console.WriteLine("task05.3")
    Tasks.task053 list
    System.Console.WriteLine("task06")
    Tasks.task06 [] // true
    Tasks.task06 [1] // true
    Tasks.task06 [1; 1] // true
    Tasks.task06 [1; 2; 3] // false
    Tasks.task06 [1; 2; 1] // true
    Tasks.task06 [1; 2; 2; 1] // true
    Tasks.task06 [1; 2; 3; 3; 2; 1] // true
    Tasks.task06 [1; 2; 3; 4; 3; 2; 1] // true
    Tasks.task06 [1; 2; 3; 4; 5; 3; 2; 1] // false
    System.Console.WriteLine("task07")
    Tasks.task07 nestedList
    // Tasks.task07 (Tasks.List [Tasks.List []])
    Tasks.task07 nestedListChars
    //Tasks.task07 [[[1; 1]; 2]]//[[[1; 1]; 2; [[3; [5; 8]]]]
    //Tasks.task07 [List [Elem 1; List [Elem 2; List [Elem 3; Elem 4]; Elem 5]]);//[[[1; 1]; 2; [[3; [5; 8]]]]
    
   


    System.Console.ReadLine()

    0 // Exitcode aus ganzen Zahlen zurückgeben

