﻿module Tasks
    
    type 'a NestedList = List of 'a NestedList list | Elem of 'a

    let nthElement listParam index =
        List.nth listParam index
    
    let lastElement listParam =
        let rec last = function
            |[] -> failwithf "empty list!!!"
            |x::[] -> x
            |x::listParam -> last listParam
        last listParam

    let removeLastElement listParam =
        let rec removeLast = function
            | [] -> []
            | x::[] -> []
            | x :: listParam -> (removeLast listParam) @ [x]
        removeLast listParam

    let print s =
        printfn "%i" s
    
    let printList list =
        printfn "%A" list

    let task01 listParam =        
        nthElement listParam (listParam.Length - 1)
        |> print

    let task02 listParam =
        nthElement listParam (listParam.Length - 2)
        |> print

    let task03 listParam index =
        nthElement listParam (index - 1)
        |> print

    let task041 listParam =
        List.length listParam
        |> print

    let task042 listParam =
        listParam 
        |> List.sumBy(fun _-> 1)
        |> print

    let task043 listParam =
        let rec length = function
            | []->0
            | _::listParam->length listParam + 1
        length listParam
        |> print

    let task051 listParam =
        List.rev listParam
        |> printList

    let task052 listParam =
        List.fold(fun acc x -> x::acc) [] listParam
        |> printList

    // combine two lists with "@"-operator: https://msdn.microsoft.com/de-de/library/dd233224.aspx#Anchor_1
    let task053 listParam =
        let rec reverse = function
            | [] -> []
            | x::[] -> x :: []
            | x :: listParam -> (reverse listParam) @ [x]
        reverse listParam
        |> printList

    let task06 listParam =
        let rec isPalindrom = function
            | [] -> true
            | x::[] -> true
            | [x;y] -> x.Equals y
            | x::listParam -> x.Equals (lastElement(listParam)) && isPalindrom(removeLastElement(listParam))
        isPalindrom listParam
        |> printfn "%b"

    let task07 listParam =
        let rec flatten = function
            | List[] -> []
            | Elem x -> [x]
            | List(x::xs) -> flatten(x) @ flatten(List(xs))
        flatten listParam
        |> printList